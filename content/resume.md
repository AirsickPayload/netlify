+++
date = "2019-06-01T00:00:00-00:00"
title = "Resume"
categories = [
  "resume"
]
weight = 1
+++

---

## Alan Matuszczak

Poznań, Wielkopolskie, Poland  
contact [AT] alanmatuszczak.me

Statement
---
Striving for keeping things neat and automated. Aspiring to work on a large-scale infrastructure. Looking for a DevOps Engineer or Site Reliability Engineer position. Open for remote work.

Experience
---

###### _Site Reliability Engineer at OLX Group - Poznań, Poland </br></br> November 2019 - present_

to be continued...

###### _Junior Site Reliability Engineer at OLX Group - Poznań, Poland </br></br> February 2019 - October 2019_

* Took part in on-call rotations
* Perfomed online schema migrations on milliard-row Aurora MySQL tables using [gh-ost](https://github.com/github/gh-ost)
* Handled the configuration, documentation, deployment and monitoring of new staging/production RabbitMQ clusters
* Replaced certain shell-based maintenance tasks with AWS System Manager Commands
* Participated in an internal AWS EKS Workshop aiming to allow multiple teams use Kubernetes easily
* Managed AWS Resources with Terraform
* Coordinated turning on of New Relic's Distributed Tracing for 6 countries (and infrastructure stacks) OLX "Europe" operates in
* Migrated staging and production deployment processes from CodePipeline to GitLab CI to unify all CI stages in a single pipeline; feature parity was achieved using an in-house Python script
* Prepared AWS ElasticBeanstalk environments, Aurora MySQL clusters, CI pipelines and isolated VPCs for a CMS-like portal for the 'Jobs' classifieds category
* Contributed to EU Shared EKS cluster internal components configuration: logging (fluentd), DNS (external-dns with Route53), RBAC (custom Helm chart to manage user access)VPCLink-available internal LB (based on nginx-ingress) and monitoring (both Prometheus and SignalFx)
* Debugged, fixed and maintained EKS environments for three different packs/teams
* Provided input to the Jobs team's backend architecture and have set up auxiliary services (AWS API Gateway and DocumentDB/MongoDB) for it


to be continued...

###### _DevOps Engineer at Flyps - Poznań, Poland  </br></br>September 2018 - February 2019_

Main goals:

* reducing the developers worries by moving the application stack to a stable cloud-based infrastructure
* pioneering a version-controlled stack configuration on Kubernetes

Detailed actions:

* Migrated a Rancher-based, on-premise production infrastructure to Google Kubernetes Engine
* Wrote a series of Helm charts managing the microservice stack
* Simplified the deployment of Helm charts using Helmfile
* Assessed Azure Kubernetes Service as a production deployment target
* Introduced Prometheus as the central monitoring solution
* Offloaded basic uptime checks to Stackdriver Monitoring
* Managed an Elastic Cloud instance and pushed logs to it using Filebeat and Logstash
* Performed an evaluation deployment of Apache Kafka using the Strimzi operator + benchmarking
* Wrote Prometheus rules and alerts
* Perfomed Kubernetes resource-based management of DNS (external-dns) and SSL (cert-manager)
* Made secrets encrypted using Google Cloud KMS and Mozilla's sops
* Reduced maintenance overhead by migrating CI jobs from Jenkins to Gitlab CI
* Used Terraform to:
  * manage static DNS entries (Cloudflare)
  * setup networking, firewalls, storage, IAM and Kubernetes clusters in the cloud (Google Cloud Platform and Microsoft Azure)
  * simplify management of multiple GCP project and Kubernetes clusters by grouping resources in Terraform modules
  * deploy highly available MySQL (CloudSQL) instances

###### _DevOps Engineer at Egnyte - Poznań, Poland</br></br>  March 2018 - August 2018_

* Prepared the infrastructure and CI/CD process for a new video transcoding service, deployed on GKE
* Brought immutable VMs into existing legacy CI pipelines

###### _Junior Build Release at Egnyte---Poznań, Poland</br></br>  July 2016 - February 2017_

Practical experience with:

  * setup, deployment and maintenance of:
    * internet-facing applications (Sonatype Nexus, Jenkins, AWX)
    * packaging (with additional signing and hosting of repositories)
    * on-premise VM appliances (Alpine/CentOS/Debian-based)
    * Cloud-managed (GKE) Kubernetes clusters
    * CI servers (Jenkins, Gitlab) and nodes (QA and production environments)
  * automatization of OS install and setup processes (Vagrant, Packer)
  * OS scripting
  * Configuration management tools:
    * Ansible - automation of regular maintenance tasks; helper roles for CI integration of OSX and Windows VMs
    * Puppet - managing CI Linux boxes, improvement of existing modules
  * Hypervisors/Virtualization (ESXi, Virtualbox)
  * Infrastructure as Code (Terraform)
  * server applications debugging
  * monitoring (Kibana, Nagios and New Relic)
  * containerization of services
  * writing and improving Continuous Integration/Delivery pipelines
  * configuring web servers (nginx)
  * networking (basic)
  * managing CI jobs in the following ecosystems and languages:
    * Android
    * iOS/OSX
    * C++ (Windows)
    * C#
    * Python
    * Go
    * Ruby

###### _Build Release Intern at Egnyte - Poznań, Poland</br></br>  February 2016 - June 2016_

* Automated Jenkins agents configuration using Puppet and Ansible.
* Wrote and improved Continuous Integration/Delivery pipelines
* Increased the stability and performance of production Jenkins server by setting up backups, tweaking the JVM and automating the process of setting up new instances for testing purposes
